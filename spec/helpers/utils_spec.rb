require_relative '../../helpers/utils'

RSpec.describe Utils do
  describe 'when call valid_entry' do
    context 'with number out of range' do
      let(:range) {(1..5)}
      let(:number) {rand(6..9).to_s}
      it 'should return false' do
        expect(Utils.valid_entry? number, range).to be false
      end
    end
    context 'with number inside of range' do
      let(:range) {(1..5)}
      let(:number) {rand(1..5).to_s}
      it 'should return true' do
        expect(Utils.valid_entry? number, range).to be true
      end
    end
  end

  describe 'when call clear_screen' do
    context 'in right system' do
      before :each do
        allow(Gem).to receive('win_platform?').and_return(true)
      end
      it 'should return true' do
        expect_any_instance_of(Kernel).to receive(:system).with 'cls'
        Utils.clear_screen
      end
    end

    context 'in wrong system' do
      before :each do
        allow(Gem).to receive('win_platform?').and_return(false)
      end
      it 'should return nil' do
        expect_any_instance_of(Kernel).to receive(:system).with 'clear'
        Utils.clear_screen
      end
    end
  end
end