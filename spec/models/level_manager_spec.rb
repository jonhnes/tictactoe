require_relative '../../models/level_manager'

RSpec.describe LevelManager do
  before :each do
    @level_manager = LevelManager.new
  end

  describe 'when call get_game_level' do
    context 'with right param' do
      let(:opponent_type){rand(0..1)}
      before :each do
        allow($stdin).to receive(:gets).and_return(opponent_type.to_s)
      end
      it 'should return it' do
        expect(@level_manager.get_game_level).to be opponent_type
      end
    end

    context 'with wrong and after right param' do
      let(:wrong_opponent_type){rand(3..9)}
      let(:right_opponent_type){rand(0..2)}
      before :each do
        allow($stdin).to receive(:gets).and_return(wrong_opponent_type.to_s, right_opponent_type.to_s)
      end
      it 'should return just right param' do
        expect(@level_manager.get_game_level).to be right_opponent_type
      end
    end
  end

  describe 'when call print_level_screen' do
    context 'and has error message' do
      let(:error_message) {'Test message error'}
      let(:screen_string) {
        "TIC TAC TOE\n" +
        "Choose a game level\n" +
        "Easy[0] Medium[1] Hard[2]\n"
      }
      it 'should print screen with error message' do
        expect do
          @level_manager.print_level_screen error_message
        end.to output(screen_string + "#{error_message}\n").to_stdout
      end
    end

    context 'and has not error message' do
      let(:screen_string) {
        "TIC TAC TOE\n" +
        "Choose a game level\n" +
        "Easy[0] Medium[1] Hard[2]\n"
      }
      it 'should not print screen with error message' do
        expect do
          @level_manager.print_level_screen
        end.to output(screen_string).to_stdout
      end
    end
  end
end