require_relative '../../models/opponent_manager'

RSpec.describe OpponentManager do
  before :each do
    @opponent_manager = OpponentManager.new
  end

  describe 'when call get_opponent_type' do
    context 'with right param' do
      let(:opponent_type){rand(0..1)}
      before :each do
        allow($stdin).to receive(:gets).and_return(opponent_type.to_s)
      end
      it 'should return it' do
        expect(@opponent_manager.get_opponent_type).to be opponent_type
      end
    end

    context 'with wrong and after right param' do
      let(:wrong_opponent_type){rand(3..9)}
      let(:right_opponent_type){rand(0..2)}
      before :each do
        allow($stdin).to receive(:gets).and_return(wrong_opponent_type.to_s, right_opponent_type.to_s)
      end
      it 'should return just right param' do
        expect(@opponent_manager.get_opponent_type).to be right_opponent_type
      end
    end
  end

  describe 'when call print_opponent_choose_screen' do
    context 'and has error message' do
      let(:error_message) {'Test message error'}
      let(:screen_string) {
        "TIC TAC TOE\n" +
        "Choose opponent type\n" +
            "Human vs Human[0] Human vs Computer[1] Computer vs Computer[2]\n"
      }
      it 'should print game with commands' do
        expect do
          @opponent_manager.print_opponent_choose_screen error_message
        end.to output(screen_string + "#{error_message}\n").to_stdout
      end
    end

    context 'and has not error message' do
      let(:screen_string) {
        "TIC TAC TOE\n" +
        "Choose opponent type\n" +
            "Human vs Human[0] Human vs Computer[1] Computer vs Computer[2]\n"
      }
      it 'should print game with commands' do
        expect do
          @opponent_manager.print_opponent_choose_screen
        end.to output(screen_string).to_stdout
      end
    end
  end
end