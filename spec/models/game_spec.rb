require_relative '../../models/game'

RSpec.describe 'Game' do
  before :each do
    @game = Game.new
  end

  describe 'when call get_human_spot' do
    context 'with valid entry' do
      let(:board) {
        board = (0..8).map(&:to_s)
        (0..7).each do
          spot = @game.spots_available(board).sample.to_i
          board[spot] = %w(X O).sample
        end
        board
      }
      let(:spot_available) {@game.spots_available(board).first}
      before :each do
        allow($stdin).to receive(:gets).and_return(spot_available)
      end
      it 'should set "O" in spot' do
        @game.get_human_spot board, %w(X O).sample
        expect(@game.spots_available board).to be_empty
      end
    end
    context 'with already used spot' do
      let(:board) {
        board = (0..8).map(&:to_s)
        (0..7).each do
          spot = @game.spots_available(board).sample.to_i
          board[spot] = %w(X O).sample
        end
        board
      }
      let(:spot_available) {@game.spots_available(board).first.to_s}
      let(:used_spot) {
        spot_available = @game.spots_available(board).first
        (0..8).to_a.select {|a| a != spot_available}.sample
      }
      let(:rival_symbol) {board[used_spot] == 'X' ? 'O' : 'X'}
      before :each do
        allow($stdin).to receive(:gets).and_return(used_spot.to_s, spot_available.to_s)
      end
      it 'should not change spot' do
        @game.get_human_spot board, rival_symbol
        expect(board[used_spot]).to_not be == rival_symbol
      end
    end
  end

  describe 'when call eval_board' do
    context 'with 4 spot empty' do
      let(:board) {
        board = (0..8).map(&:to_s)
        spot = [0, 1, 2, 3, 5, 6, 7, 8].sample
        board[spot] = %w(X O).sample
        board
      }
      it 'should put computer symbol their' do
        @game.eval_board board, 'X', rand(0..2)
        expect(board[4]).to be == 'X'
      end
    end
    context 'with 4 spot not empty' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[4] = %w(X O).sample
        (0..3).each do
          spot = @game.spots_available(board).sample.to_i
          board[spot] = %w(X O).sample
        end
        board
      }
      let(:best_move) {@game.get_best_move board, 'X', 'O'}
      it 'should get best move' do
        best_move
        @game.eval_board board, 'X', rand(1..2)
        expect(board[best_move]).to be == 'X'
      end
    end
  end

  describe 'when call get_best_move' do
    context 'and the computer player' do
      context 'with first line all must closed' do
        let(:first_line) {[0, 1, 2]}
        let(:occupied_spot) {first_line.sample 2}
        let(:available_spot) {(first_line - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with second line all must closed' do
        let(:second_line) {[0, 1, 2]}
        let(:occupied_spot) {second_line.sample 2}
        let(:available_spot) {(second_line - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with third line all must closed' do
        let(:third_line) {[0, 1, 2]}
        let(:occupied_spot) {third_line.sample 2}
        let(:available_spot) {(third_line - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with first column all must closed' do
        let(:first_column) {[0, 3, 6]}
        let(:occupied_spot) {first_column.sample 2}
        let(:available_spot) {(first_column - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with second column all must closed' do
        let(:second_column) {[1, 4, 7]}
        let(:occupied_spot) {second_column.sample 2}
        let(:available_spot) {(second_column - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with third column all must closed' do
        let(:third_column) {[2, 5, 8]}
        let(:occupied_spot) {third_column.sample 2}
        let(:available_spot) {(third_column - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with main diagonal all must closed' do
        let(:main_diagonal) {[0, 4, 8]}
        let(:occupied_spot) {main_diagonal.sample 2}
        let(:available_spot) {(main_diagonal - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with secondary diagonal all must closed' do
        let(:secondary_diagonal) {[2, 5, 8]}
        let(:occupied_spot) {secondary_diagonal.sample 2}
        let(:available_spot) {(secondary_diagonal - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'X'
          end
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with any move' do
        let(:available_spot) {(secondary_diagonal - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          board[rand(0..8)] = 'X'
          board
        }
        it 'should win game' do
          best_move = @game.get_best_move(board, 'O', 'X').to_s
          expect(@game.spots_available board).to include best_move
        end
      end
    end
    context 'and the human player' do
      context 'with first line all must closed' do
        let(:first_line) {[0, 1, 2]}
        let(:occupied_spot) {first_line.sample 2}
        let(:available_spot) {(first_line - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with second line all must closed' do
        let(:second_line) {[0, 1, 2]}
        let(:occupied_spot) {second_line.sample 2}
        let(:available_spot) {(second_line - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with third line all must closed' do
        let(:third_line) {[0, 1, 2]}
        let(:occupied_spot) {third_line.sample 2}
        let(:available_spot) {(third_line - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with first column all must closed' do
        let(:first_column) {[0, 3, 6]}
        let(:occupied_spot) {first_column.sample 2}
        let(:available_spot) {(first_column - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with second column all must closed' do
        let(:second_column) {[1, 4, 7]}
        let(:occupied_spot) {second_column.sample 2}
        let(:available_spot) {(second_column - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with third column all must closed' do
        let(:third_column) {[2, 5, 8]}
        let(:occupied_spot) {third_column.sample 2}
        let(:available_spot) {(third_column - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with main diagonal all must closed' do
        let(:main_diagonal) {[0, 4, 8]}
        let(:occupied_spot) {main_diagonal.sample 2}
        let(:available_spot) {(main_diagonal - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
      context 'with secondary diagonal all must closed' do
        let(:secondary_diagonal) {[2, 5, 8]}
        let(:occupied_spot) {secondary_diagonal.sample 2}
        let(:available_spot) {(secondary_diagonal - occupied_spot).first}
        let(:board) {
          board = (0..8).map(&:to_s)
          occupied_spot.each do |spot|
            board[spot] = 'O'
          end
          board
        }
        it 'should not lose the game' do
          best_move = @game.get_best_move board, 'O', 'X'
          expect(best_move).to be available_spot
        end
      end
    end
  end

  describe 'when call game_is_over?' do
    context 'with first line occupied with same symbol' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[0] = board[1] = board[2] = %w(X O).sample
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
    context 'with second line occupied with same symbol' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[3] = board[4] = board[5] = %w(X O).sample
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
    context 'with third line occupied with same symbol' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[6] = board[7] = board[8] = %w(X O).sample
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
    context 'with first column occupied with same symbol' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[0] = board[3] = board[6] = %w(X O).sample
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
    context 'with second column occupied with same symbol' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[1] = board[4] = board[7] = %w(X O).sample
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
    context 'with third column occupied with same symbol' do
      let(:board) {
        board = (0..8).map(&:to_s)
        board[2] = board[5] = board[8] = %w(X O).sample
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
    context 'with tie game' do
      let(:board) {
        board = []
        board[0] = board[4] = board[5] = board[6] = 'X'
        board[1] = board[2] = board[3] = board[7] = board[8] = 'O'
      }
      it 'should return true' do
        expect(@game.game_is_over? board).to be true
      end
    end
  end

  describe 'when call all_spots_occupied?' do
    context 'without spot available' do
      let(:board) {(0..8).map {%w(X O).sample}}
      it 'should return true' do
        expect(@game.all_spots_occupied? board).to be true
      end
    end
    context 'with spot available' do
      let(:board) {
        board = (0..8).map {%w(X O).sample}
        spot = rand(0..8)
        board[spot] = spot.to_s
        board
      }
      it 'should return false' do
        expect(@game.all_spots_occupied? board).to be false
      end
    end
  end

  describe 'when call print_actual_game' do
    context 'and game is not over' do
      let(:board) {(0..8).map(&:to_s)}
      let(:board_string) {
            "TIC TAC TOE\n" +
            " #{board[0]} | #{board[1]} | #{board[2]} \n===+===+===\n" +
            " #{board[3]} | #{board[4]} | #{board[5]} \n===+===+===\n" +
            " #{board[6]} | #{board[7]} | #{board[8]} \n"
      }
      it 'should print game with commands' do
        expect do
          @game.print_actual_game board
        end.to output(board_string + "Enter [0-8]:\n").to_stdout
      end
    end

    context 'and game is over' do
      let(:board) {(0..8).map {%w(X O).sample}}
      let(:board_string) {
            "TIC TAC TOE\n" +
            " #{board[0]} | #{board[1]} | #{board[2]} \n===+===+===\n" +
            " #{board[3]} | #{board[4]} | #{board[5]} \n===+===+===\n" +
            " #{board[6]} | #{board[7]} | #{board[8]} \n"
      }
      it 'should print game over' do
        expect do
          @game.print_actual_game board
        end.to output(board_string + "Game over\n").to_stdout
      end
    end
  end

  describe 'when call is_a_valid_spot' do
    context 'with X or O value' do
      let(:spot_value) {%w(X O).sample}
      it 'should return false' do
        expect(@game.is_a_valid_spot? spot_value).to be false
      end
    end
    context 'with number value' do
      let(:spot_value) {rand(0..8).to_s}
      it 'should return false' do
        expect(@game.is_a_valid_spot? spot_value).to be true
      end
    end
  end

  describe 'when call spots_available' do
    let(:spots_available_count) {rand(1..8)}
    let(:board) {
      board = (0..8).map(&:to_s)
      (0..(8-spots_available_count)).each do
        spot = @game.spots_available(board).sample.to_i
        board[spot] = %w(X O).sample
      end
      board
    }
    it 'should return unused spots' do
      expect(@game.spots_available(board).count).to be spots_available_count
    end
  end
end
