require './models/opponent_manager'
require './models/level_manager'

class Game
  def initialize
    @board = %w(0 1 2 3 4 5 6 7 8)
    @type = nil
    @level = nil
    @opponent_screen = OpponentManager.new
    @level_screen = LevelManager.new
    @player_one = 'O' # the user's marker
    @player_two = 'X' # the computer's marker
  end

  def start_game
    @type = @opponent_screen.get_opponent_type
    @level = @level_screen.get_game_level if @type == OpponentManager::HUMAN_COMPUTER or @type == OpponentManager::COMPUTER_COMPUTER
    print_actual_game @board
    until game_is_over? @board
      if @type == OpponentManager::HUMAN_HUMAN or @type == OpponentManager::HUMAN_COMPUTER
        get_human_spot @board, @player_one
      else
        sleep(2)
        eval_board @board, @player_one, @level unless game_is_over? @board
      end
      if @type == OpponentManager::HUMAN_COMPUTER or @type == OpponentManager::COMPUTER_COMPUTER
        eval_board @board, @player_two, @level unless game_is_over? @board
      else
        print_actual_game @board
        get_human_spot @board, @player_two unless game_is_over? @board
      end
      print_actual_game @board
    end
  end

  def get_human_spot(board, player_symbol)
    spot = nil
    until spot
      spot = $stdin.gets.chomp.to_i
      if is_a_valid_spot? board[spot] and Utils.valid_entry?(spot, (0..8))
        board[spot] = player_symbol
      else
        print_actual_game board, 'Invalid entry try again'
        spot = nil
      end
    end
  end

  def eval_board(board, player_symbol, level)
    return board[4] = player_symbol if is_a_valid_spot?(board[4])
    case level
      when LevelManager::EASY
        spot = spots_available(board).sample.to_i
      when LevelManager::MEDIUM
        spot = get_best_move(board, @player_two, @player_one)
      when LevelManager::HARD
        spot = get_best_move(board, @player_two, @player_one, true)
      else
        spot = get_best_move(board, @player_two, @player_one, true)
    end
    board[spot] = player_symbol
  end

  def get_best_move(board, player_two, player_one, hard_mode=false)
    available_spaces = spots_available board
    board_clone = board.clone
    available_spaces.each do |spot|
      board_clone[spot.to_i] = player_two
      return spot.to_i if game_is_over?(board_clone)
      board_clone[spot.to_i] = player_one
      return spot.to_i if game_is_over?(board_clone)
      board_clone[spot.to_i] = spot
    end
    return (available_spaces - %w(1 3 5 7)).sample.to_i if hard_mode
    available_spaces.sample.to_i
  end

  def game_is_over?(b)
    [b[0], b[1], b[2]].uniq.length == 1 or
    [b[3], b[4], b[5]].uniq.length == 1 or
    [b[6], b[7], b[8]].uniq.length == 1 or
    [b[0], b[3], b[6]].uniq.length == 1 or
    [b[1], b[4], b[7]].uniq.length == 1 or
    [b[2], b[5], b[8]].uniq.length == 1 or
    [b[0], b[4], b[8]].uniq.length == 1 or
    [b[2], b[4], b[6]].uniq.length == 1 or
        all_spots_occupied?(b)
  end

  def all_spots_occupied?(board)
    board.all? {|spot| not is_a_valid_spot? spot}
  end

  def print_actual_game(board, error_message=nil)
    Utils.clear_screen
    puts "TIC TAC TOE\n"
    puts     " #{board[0]} | #{board[1]} | #{board[2]} \n===+===+===\n" +
             " #{board[3]} | #{board[4]} | #{board[5]} \n===+===+===\n" +
             " #{board[6]} | #{board[7]} | #{board[8]} \n"
    puts game_is_over?(board) ? 'Game over' : 'Enter [0-8]:'
    puts "#{error_message}\n" if error_message
  end

  def is_a_valid_spot?(spot_value)
    not /[XO]/.match(spot_value)
  end

  def spots_available(board)
    available_spaces = []
    board.each do |spot|
      available_spaces.push spot if is_a_valid_spot? spot
    end
    available_spaces
  end
end