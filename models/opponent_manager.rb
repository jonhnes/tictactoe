require_relative './../helpers/utils'
class OpponentManager
  HUMAN_HUMAN = 0
  HUMAN_COMPUTER = 1
  COMPUTER_COMPUTER = 2

  def get_opponent_type
    print_opponent_choose_screen
    loop do
      @type = $stdin.gets.chomp
      Utils.valid_entry?(@type, 0..2) ? break : print_opponent_choose_screen('Invalid entry try again')
    end
    @type.to_i
  end

  def print_opponent_choose_screen(error_message=nil)
    Utils.clear_screen
    puts "TIC TAC TOE\n"
    puts "Choose opponent type\n"
    puts "Human vs Human[0] Human vs Computer[1] Computer vs Computer[2]\n"
    puts "#{error_message}\n" if error_message
  end
end