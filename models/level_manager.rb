require_relative './../helpers/utils'
class LevelManager
  EASY = 0
  MEDIUM = 1
  HARD = 2
  def get_game_level
    print_level_screen
    loop do
      @type = $stdin.gets.chomp
      Utils.valid_entry?(@type, 0..2) ? break : print_level_screen('Invalid entry try again')
    end
    @type.to_i
  end

  def print_level_screen(error_message=nil)
    Utils.clear_screen
    puts "TIC TAC TOE\n"
    puts "Choose a game level\n"
    puts "Easy[0] Medium[1] Hard[2]\n"
    puts "#{error_message}\n" if error_message
  end
end