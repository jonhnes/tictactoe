module Utils
  def self.valid_entry?(string, range)
    number = Integer(string) rescue nil
    range.include? number
  end

  def self.clear_screen
    Gem.win_platform? ? (system 'cls') : (system 'clear')
  end
end